<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Title</title>
</head>
<body>
    <h2>Latihan PHP</h2>
    <?php
        echo "<h3>Contoh Soal 1</h3>";
        $Kalimat1 = "PHP is never old";
        echo "Kalimat1 : " . $Kalimat1 . "<br>";
        echo "Panjang string : " . strlen($Kalimat1) . "<br>";
        echo "Jumlah Kata : " . str_word_count($Kalimat1) . "<br>";

        echo "<h3>Soal 2</h3>";
        $Kalimat2 = "I love PHP";
        echo "Kalimat : " . $Kalimat2 . "<br>";
        echo "Kata 1 : ". substr($Kalimat2,0,1)."<br>";
        echo "Kata 2 : ". substr($Kalimat2,2,5)."<br>";
        echo "Kata 3 : ". substr($Kalimat2,7,9)."<br>";

        echo "<h3>Soal 3</h3>";
        $Kalimat3 = "PHP is old but Good!";
        echo "Kalimat 3 : " . $Kalimat3 . "<br>";
        echo "Kalimat 3 di ubah : " . str_replace("Good","Awesome",$Kalimat3);
    ?>
</body>
</html>